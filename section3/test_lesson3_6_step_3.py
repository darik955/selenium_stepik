import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import math
import time

@pytest.fixture(scope="function")
def browser():
    print("\nstart browser for test..")
    browser = webdriver.Chrome()
    yield browser
    print("\nquit browser..")
    browser.quit()

@pytest.mark.parametrize('number', ["236895", "236896", "236897", "236898", "236899", "236903", "236904", "236905"])
@pytest.mark.xfail()
def test_guest_should_see_login_link(browser, number):
    answer = str(math.log(int(time.time())))
    link = f"https://stepik.org/lesson/{number}/step/1"
    browser.get(link)

    WebDriverWait(browser, 60).until(EC.visibility_of_element_located((By.ID, "ember66")))

    browser.find_element_by_id("ember66").click()

    WebDriverWait(browser, 60).until(EC.visibility_of_element_located((By.ID, "ember67")))

    input1 = browser.find_element_by_id("ember67")
    input1.send_keys(answer)

    WebDriverWait(browser, 60).until(EC.visibility_of_element_located((By.CLASS_NAME, "submit-submission")))

    # нажимаем на Отправить
    button1 = browser.find_element_by_class_name("submit-submission")
    button1.click()

    WebDriverWait(browser, 60).until(EC.visibility_of_element_located((By.CLASS_NAME, "smart-hints__hint")))

    #Correct проверка
    # находим элемент, содержащий текст
    answer_text_elt = browser.find_element_by_class_name("smart-hints__hint")

    # записываем в переменную welcome_text текст из элемента answer_text_elt
    answer_text = answer_text_elt.text

    # с помощью assert проверяем, что ожидаемый текст совпадает с текстом на странице сайта
    assert "Correct!" == answer_text, print(answer_text)
    time.sleep(3)

