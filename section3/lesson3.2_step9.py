s = 'My Name is Julia'

if 'Name' in s:
    print('Substring found')

index = s.find('Name')
if index != -1:
    print(f'Substring found at index {index}')

assert "NameName" in s, "Not found element"

def test_substring(full_string, substring):
    assert substring in full_string, "expected '" + substring + "' to be substring of '" + full_string + "'"