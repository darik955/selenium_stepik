#запускать через консоль python lesson3.2_step13.py

import unittest
from selenium import webdriver
import time

class Test_step11(unittest.TestCase):
    def test_good(self):
        try:
            link = "http://suninjuly.github.io/registration1.html"
            browser = webdriver.Chrome()
            browser.get(link)

            # Ваш код, который заполняет обязательные поля
            input1 = browser.find_element_by_class_name("form-control.first")
            input1.send_keys("Darya")

            input2 = browser.find_element_by_xpath("//input[@placeholder='Input your last name']")
            input2.send_keys("Polyakova")

            input3 = browser.find_element_by_class_name("form-control.third")
            input3.send_keys("darik95@list.ru")

            time.sleep(5)

            # Отправляем заполненную форму
            button = browser.find_element_by_css_selector("button.btn")
            button.click()

            # Проверяем, что смогли зарегистрироваться
            # ждем загрузки страницы
            time.sleep(1)

            # находим элемент, содержащий текст
            welcome_text_elt = browser.find_element_by_tag_name("h1")
            # записываем в переменную welcome_text текст из элемента welcome_text_elt
            welcome_text = welcome_text_elt.text

        finally:
            # ожидание чтобы визуально оценить результаты прохождения скрипта
            time.sleep(3)
            # закрываем браузер после всех манипуляций
            browser.quit()
        self.assertEqual(welcome_text, "Congratulations! You have successfully registered!")

    def test_neg(self):
        try:
            link = "http://suninjuly.github.io/registration2.html"
            browser = webdriver.Chrome()
            browser.get(link)

            # Ваш код, который заполняет обязательные поля
            input1 = browser.find_element_by_class_name("form-control.first")
            input1.send_keys("Darya")

            input2 = browser.find_element_by_xpath("//input[@placeholder='Input your last name']")
            input2.send_keys("Polyakova")

            input3 = browser.find_element_by_class_name("form-control.third")
            input3.send_keys("darik95@list.ru")

            time.sleep(5)

            # Отправляем заполненную форму
            button = browser.find_element_by_css_selector("button.btn")
            button.click()

            # Проверяем, что смогли зарегистрироваться
            # ждем загрузки страницы
            time.sleep(1)

            # находим элемент, содержащий текст
            welcome_text_elt = browser.find_element_by_tag_name("h1")
            # записываем в переменную welcome_text текст из элемента welcome_text_elt
            welcome_text = welcome_text_elt.text

        finally:
            # ожидание чтобы визуально оценить результаты прохождения скрипта
            time.sleep(3)
            # закрываем браузер после всех манипуляций
            browser.quit()
        self.assertEqual(welcome_text, "Congratulations! You have successfully registered!")


if __name__ == "__main__":
    unittest.main()