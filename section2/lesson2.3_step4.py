from selenium import webdriver
import time
import math

def calc(x):
    return str(math.log(abs(12 * math.sin(int(x)))))

try:
    browser = webdriver.Chrome()
    link = "http://suninjuly.github.io/alert_accept.html"
    browser.get(link)

    # нажимаем на button
    button1 = browser.find_element_by_css_selector("button.btn")
    button1.click()

    time.sleep(3)

    # нажимаем ок
    alert = browser.switch_to.alert
    alert.accept()

    # считываем переменную х
    x_element = browser.find_element_by_xpath("//span[@id='input_value']")
    x = x_element.text
    y = calc(x)

    # записываем в поле
    input1 = browser.find_element_by_id("answer")
    input1.send_keys(y)

    # нажимаем на Submit
    button2 = browser.find_element_by_css_selector("button.btn")
    button2.click()

    time.sleep(3)
    # нажимаем ок
    alert = browser.switch_to.alert
    alert.accept()

finally:
    # ожидание чтобы визуально оценить результаты прохождения скрипта
    time.sleep(5)
    # закрываем браузер после всех манипуляций
    browser.quit()


