from selenium import webdriver
import time
import math


def calc(x):
    return str(math.log(abs(12 * math.sin(int(x)))))

try:
    browser = webdriver.Chrome()
    link = "http://suninjuly.github.io/execute_script.html"
    browser.get(link)

    #считываем переменную х
    x_element = browser.find_element_by_xpath("//span[@id='input_value']")
    x = x_element.text
    y = calc(x)

    #скролим вниз
    field = browser.find_element_by_id("answer")
    browser.execute_script("return arguments[0].scrollIntoView(true);", field)

    # записываем в поле
    input1 = browser.find_element_by_id("answer")
    input1.send_keys(y)

    # отмечаем checkbox
    checkbox1 = browser.find_element_by_id("robotCheckbox")
    checkbox1.click()

    # отмечаем radiobutton
    radiobutton1 = browser.find_element_by_id("robotsRule")
    radiobutton1.click()

    # нажимаем на Submit
    button1 = browser.find_element_by_css_selector("button.btn")
    button1.click()

finally:
    # ожидание чтобы визуально оценить результаты прохождения скрипта
    time.sleep(5)
    # закрываем браузер после всех манипуляций
    browser.quit()