from selenium import webdriver
import time
import math


def calc(x):
    return str(math.log(abs(12 * math.sin(int(x)))))

try:
    link = "http://suninjuly.github.io/math.html"
    browser = webdriver.Chrome()
    browser.get(link)

    #считываем переменную х
    x_element = browser.find_element_by_xpath("//span[@id='input_value']")
    x = x_element.text
    y = calc(x)

    #записываем в поле
    input1 = browser.find_element_by_id("answer")
    input1.send_keys(y)

    #проверка radiobutton
    people_radio = browser.find_element_by_id("peopleRule")
    people_checked = people_radio.get_attribute("checked")
    print("value of people radio: ", people_checked)
    assert people_checked is not None, "People radio is not selected by default"

    robots_radio = browser.find_element_by_id("robotsRule")
    robots_checked = robots_radio.get_attribute("checked")
    print("value of robots radio: ", robots_checked)
    assert robots_checked is None

    # отмечаем checkbox
    checkbox1 = browser.find_element_by_id("robotCheckbox")
    checkbox1.click()

    #отмечаем radiobutton
    radiobutton1 = browser.find_element_by_id("robotsRule")
    radiobutton1.click()

    #нажимаем на Submit
    button1 = browser.find_element_by_css_selector("button.btn")
    button1.click()


finally:
    # ожидание чтобы визуально оценить результаты прохождения скрипта
    time.sleep(10)
    # закрываем браузер после всех манипуляций
    browser.quit()
