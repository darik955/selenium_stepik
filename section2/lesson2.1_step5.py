from selenium import webdriver
import time
import math


def calc(x):
    return str(math.log(abs(12 * math.sin(int(x)))))

try:
    link = "http://suninjuly.github.io/math.html"
    browser = webdriver.Chrome()
    browser.get(link)

    #считываем переменную х
    x_element = browser.find_element_by_xpath("//span[@id='input_value']")
    x = x_element.text
    y = calc(x)

    #записыаем в поле
    input1 = browser.find_element_by_id("answer")
    input1.send_keys(y)

    # отмечаем checkbox
    checkbox1 = browser.find_element_by_id("robotCheckbox")
    checkbox1.click()

    #отмечаем radiobutton
    radiobutton1 = browser.find_element_by_id("robotsRule")
    radiobutton1.click()

    #нажимаем на Submit
    button1 = browser.find_element_by_css_selector("button.btn")
    button1.click()


finally:
    # ожидание чтобы визуально оценить результаты прохождения скрипта
    time.sleep(10)
    # закрываем браузер после всех манипуляций
    browser.quit()
