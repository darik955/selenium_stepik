from selenium import webdriver
import time
import math
import os

try:
    browser = webdriver.Chrome()
    link = "http://suninjuly.github.io/file_input.html"
    browser.get(link)

    # записываем в поле first name
    input1 = browser.find_element_by_xpath("//input[@placeholder='Enter first name']")
    input1.send_keys("Dasha")

    # записываем в поле last name
    input2 = browser.find_element_by_xpath("//input[@placeholder='Enter last name']")
    input2.send_keys("Antonova")

    # записываем в поле mail
    input3 = browser.find_element_by_xpath("//input[@placeholder='Enter email']")
    input3.send_keys("darik95@list.ru")

    #загружаем файл
    current_dir = os.path.abspath(os.path.dirname(__file__))
    file_path = os.path.join(current_dir, 'file.txt')
    element = browser.find_element_by_xpath("//input[@id='file']")
    element.send_keys(file_path)

    # нажимаем на Submit
    button1 = browser.find_element_by_css_selector("button.btn")
    button1.click()

finally:
    # ожидание чтобы визуально оценить результаты прохождения скрипта
    time.sleep(10)
    # закрываем браузер после всех манипуляций
    browser.quit()


