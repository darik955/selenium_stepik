from selenium import webdriver
from selenium.webdriver.support.ui import Select
import time

try:
    link = "http://suninjuly.github.io/selects1.html"
    browser = webdriver.Chrome()
    browser.get(link)

    num1 = browser.find_element_by_id("num1")
    num1 = int(num1.text)
    print(num1)

    num2 = browser.find_element_by_id("num2")
    num2 = int(num2.text)
    print(num2)

    y = str(num1 + num2)
    print(y)

    select = Select(browser.find_element_by_tag_name("select"))
    select.select_by_value(y)

    #нажимаем на Submit
    button1 = browser.find_element_by_css_selector("button.btn")
    button1.click()

finally:
    # ожидание чтобы визуально оценить результаты прохождения скрипта
    time.sleep(10)
    # закрываем браузер после всех манипуляций
    browser.quit()
