from selenium import webdriver
import time
import math

def calc(x):
    return str(math.log(abs(12 * math.sin(int(x)))))

try:
    browser = webdriver.Chrome()
    link = "http://suninjuly.github.io/redirect_accept.html"
    browser.get(link)

    # нажимаем на button
    button1 = browser.find_element_by_css_selector("button.trollface")
    button1.click()

    #переходим на вторую вкладку
    window_name0 = browser.window_handles[0]
    window_name1 = browser.window_handles[1]
    browser.switch_to.window(window_name1)

    # считываем переменную х
    x_element = browser.find_element_by_xpath("//span[@id='input_value']")
    x = x_element.text
    y = calc(x)

    # записываем в поле
    input1 = browser.find_element_by_id("answer")
    input1.send_keys(y)

    # нажимаем на Submit
    button2 = browser.find_element_by_css_selector("button.btn")
    button2.click()

finally:
    # ожидание чтобы визуально оценить результаты прохождения скрипта
    time.sleep(5)
    # закрываем браузер после всех манипуляций
    browser.quit()

